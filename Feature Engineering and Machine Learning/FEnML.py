# Importing Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Read data
df = pd.read_csv(filepath_or_buffer="Feature Engineering and Machine Learning/final_synthesis.csv", delimiter=',')


# Data Visualization
import seaborn as sns
from statsmodels.graphics.mosaicplot import mosaic

# Feature Engineering
df = pd.get_dummies(df, prefix=['OS', 'CPU'], columns=['OS', 'CPU'])
df = df.drop(columns=['OS_Others', 'CPU_Others']) #dummy variable traps
l = df.columns.tolist()
l.remove('rating')
l.append('rating')


df = df[l]
X = df.iloc[:, 1:20].values #
y = df.iloc[:, 20].values #
y = np.asarray([int(round(x)) for x in y]) # Categorize them to 0-10

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 2)

# Feature Scaling for numerical attributes only
from sklearn import preprocessing
min_max_scaler = preprocessing.MinMaxScaler()
X_train[:, 0:11] = min_max_scaler.fit_transform(X_train[:, 0:11])
X_test[:,0:11] = min_max_scaler.transform(X_test[:, 0:11])
X_train = np.around(X_train, decimals=2)
X_test = np.around(X_test, decimals=2)


# Machine Learning
# Fitting Models to the training set
classifier = []

# Fitting Decision Tree Classification to the Training set
from sklearn.tree import DecisionTreeClassifier
classifier.append(DecisionTreeClassifier(criterion = 'entropy', random_state = 0))
classifier[0].fit(X_train, y_train)

# Fitting Naive Bayes to the Training set
from sklearn.naive_bayes import GaussianNB
classifier.append(GaussianNB())
classifier[1].fit(X_train, y_train)

# Fitting K-NN to the Training set
from sklearn.neighbors import KNeighborsClassifier
classifier.append(KNeighborsClassifier(n_neighbors = 5, metric = 'minkowski', p = 2))
classifier[2].fit(X_train, y_train)

# Fitting SVM to the Training set
from sklearn.svm import SVC
classifier.append(SVC(kernel = 'linear', random_state = 0))
classifier[3].fit(X_train, y_train)

# Fitting Kernel SVM to the Training set
from sklearn.svm import SVC
classifier.append(SVC(kernel = 'rbf', random_state = 0))
classifier[4].fit(X_train, y_train)

# Fitting Random Forest Classification to the Training set
from sklearn.ensemble import RandomForestClassifier
classifier.append(RandomForestClassifier(n_estimators = 100, criterion = 'entropy', random_state = 0))
classifier[5].fit(X_train, y_train)

# Predicting the Test set results
y_pred = []
y_pred.append(classifier[0].predict(X_test))
y_pred.append(classifier[1].predict(X_test))
y_pred.append(classifier[2].predict(X_test))
y_pred.append(classifier[3].predict(X_test))
y_pred.append(classifier[4].predict(X_test))
y_pred.append(classifier[5].predict(X_test))

# Round off
y_test = [round(x/2) for x in y_test]
y_pred[0] = [round(x/2) for x in y_pred[0]]
y_pred[1] = [round(x/2) for x in y_pred[1]]
y_pred[2] = [round(x/2) for x in y_pred[2]]
y_pred[3] = [round(x/2) for x in y_pred[3]]
y_pred[4] = [round(x/2) for x in y_pred[4]]
y_pred[5] = [round(x/2) for x in y_pred[5]]

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = []
for x in range(0,len(y_pred)):
    cm.append(confusion_matrix(y_test, y_pred[x]))
    print(cm[x])

# Making classification_report
from sklearn.metrics import classification_report
for x in range(0,len(y_pred)):
    print(x)
    print(classification_report(y_test, y_pred[x]))

from sklearn.metrics import accuracy_score
# Make accuracy scores
for x in range(0,len(y_pred)):
    print(x)
    print(accuracy_score(y_test, y_pred[x]))