# -*- coding: utf-8 -*-
import scrapy


class CSLSpider(scrapy.Spider):
    name = 'csl'
    allowed_domains = ['www.price.com.hk']
    start_urls = ['https://www.price.com.hk/category.php?c=100005&gp=10&page=1']

    def parse(self, response):

        items = response.xpath('//*[@class="item club-list-row"]')
        for item in items:
            name = item.xpath('.//*[@class="line line-01"]/a/text()').extract()
            rating = item.xpath('.//*[@class="line line-03"]//img/@alt').extract()
            launch_date = item.xpath('.//*[@class="item-attr item-attr-00"]//*[@class="attr-info"]/span/text()').extract()
            spec_name_1 = item.xpath('.//*[@class="item-attr item-attr-01"]//*[@class="attr-label"]/text()').extract()
            spec_value_1 = item.xpath('.//*[@class="item-attr item-attr-01"]//*[@class="attr-info text-overflow"]/span/text()').extract()
            spec_name_2 = item.xpath('.//*[@class="item-attr item-attr-02"]//*[@class="attr-label"]/text()').extract()
            spec_value_2 = item.xpath('.//*[@class="item-attr item-attr-02"]//*[@class="attr-info text-overflow"]/span/text()').extract()

            spec_name = spec_name_1 + spec_name_2
            spec_value = spec_value_1 + spec_value_2
            spec_price = item.xpath('.//*[@class="listing-price-range"]/span/text()')[1].extract() \
                if (item.xpath('.//*[@class="listing-price-range"]/span/text()')) \
                else 0

            yield {
                'name': name,
                'rating': rating,
                'launch_date': launch_date,
                # 'spec_name': spec_name,
                # 'spec_value': spec_value,
                'spec_name_value' : dict(zip(spec_name, spec_value)),
                'price': spec_price

            }

        next_page_url = response.xpath('//*[@class="next-btn"]/a/@href').extract_first()
        absolute_next_page_url = response.urljoin(next_page_url)
        yield scrapy.Request(absolute_next_page_url)