import pandas as pd
import numpy as np

df = pd.read_csv('Data Preprocessing/Stage 4 from_OS_CPU_left_to_OS_CPU_complete_to_final_synthesis/OS_CPU_complete(synthesize_value).csv')

df['launch_date'] = [(int(df['launch_date'].sample()) + df['launch_date'].mean())/2 if s==0 else s for s in df['launch_date']]
df['price'] = [(int(df['price'].sample()) + df['price'].mean())/2 if s==0 else s for s in df['price']]
df['Display'] = [(int(df['Display'].sample()) + df['Display'].mean())/2 if s==0 else s for s in df['Display']]
df['Resolution'] = [(int(df['Resolution'].sample()) + df['Resolution'].mean())/2 if s==0 else s for s in df['Resolution']]
df['Front Camera'] = [(int(df['Front Camera'].sample()) + df['Front Camera'].mean())/2 if s==0 else s for s in df['Front Camera']]
df['Rear Camera'] = [(int(df['Rear Camera'].sample()) + df['Rear Camera'].mean())/2 if s==0 else s for s in df['Rear Camera']]
df['Storage'] = [(int(df['Storage'].sample()) + df['Storage'].mean())/2 if s==0 else s for s in df['Storage']]
df['Ram'] = [(int(df['Ram'].sample()) + df['Ram'].mean())/2 if s==0 else s for s in df['Ram']]
df['Weight'] = [(int(df['Weight'].sample()) + df['Weight'].mean())/2 if s==0 else s for s in df['Weight']]
df['Dimension'] = [(int(df['Dimension'].sample()) + df['Dimension'].mean())/2 if s==0 else s for s in df['Dimension']]
df['Battery Capacity'] = [(int(df['Battery Capacity'].sample()) + df['Battery Capacity'].mean())/2 if s==0 else s for s in df['Battery Capacity']]

# df['rating'] = [(1-(s/df['launch_date'].max()))*0.1\

df['launch_date'] = df['launch_date'].apply(pd.to_numeric, errors='coerce')
df['price'] = df['price'].apply(pd.to_numeric, errors='coerce')
df['Display'] = df['Display'].apply(pd.to_numeric, errors='coerce')
df['Resolution'] = df['Resolution'].apply(pd.to_numeric, errors='coerce')
df['Front Camera'] = df['Front Camera'].apply(pd.to_numeric, errors='coerce')
df['Rear Camera'] = df['Rear Camera'].apply(pd.to_numeric, errors='coerce')
df['Storage'] = df['Storage'].apply(pd.to_numeric, errors='coerce')
df['Ram'] = df['Ram'].apply(pd.to_numeric, errors='coerce')
df['Weight'] = df['Weight'].apply(pd.to_numeric, errors='coerce')
df['Dimension'] = df['Dimension'].apply(pd.to_numeric, errors='coerce')
df['Battery Capacity'] = df['Battery Capacity'].apply(pd.to_numeric, errors='coerce')


for index, row in df.iterrows():
    if row['rating'] == -1:
        final_rating = \
            1-row['launch_date']/df['launch_date'].max() + \
            row['price'] / df['price'].max() + \
            row['Display'] / df['Display'].max() + \
            row['Resolution'] / df['Resolution'].max() + \
            row['Front Camera'] / df['Front Camera'].max() + \
            row['Rear Camera'] / df['Rear Camera'].max() + \
            row['Storage'] / df['Storage'].max() + \
            row['Ram'] / df['Ram'].max() + \
            1 - row['Weight'] / df['Weight'].max() + \
            row['Battery Capacity'] / df['Battery Capacity'].max()
        df['rating'][index] = final_rating

df.to_csv('final_synthesis.csv', encoding='utf_8_sig', index= False)