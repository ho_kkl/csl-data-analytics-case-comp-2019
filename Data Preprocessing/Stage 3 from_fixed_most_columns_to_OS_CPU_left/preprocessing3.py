import pandas as pd
import numpy as np

df = pd.read_csv('csl_spider/from_fixed_most_columns_to_fixed_all/fixed_most_columns.csv')

df['Dimension'] = [eval(s[:-2].replace(u'\u202f', u'').replace(u'\xa0', u'').replace('x', '*').replace('mm', '').replace('X', '*').replace(',', '')) if type(s) == str and s != '0' else 0 for s in df['Dimension']]

df['Resolution'] = [eval(s.replace('x', '*').replace('X', '*').replace('×', '*')) if type(s) == str else 0 for s in df['Resolution']]

df.to_csv('OS_CPU_left.csv', encoding='utf_8_sig')

157.5 * 74.8 * 8.2
157.5 * 74.8 * 8.2
148.6  * 71.2 * 7.4
'148.6  * 71.2 * 7.4'.strip()
'157.5 * 74.8 * 8.2'.strip()
'147.3 × 71.85 × 7.24'.strip()