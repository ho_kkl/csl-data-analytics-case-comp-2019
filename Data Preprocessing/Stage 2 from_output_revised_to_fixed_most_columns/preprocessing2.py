import pandas as pd
import numpy as np
from datetime import datetime as dt

df = pd.read_csv('csl_spider/from_output_revised_to_fixed_most_columns/output_revised.csv')

df['launch_date'] = [ (dt.strptime('28/10/2019', '%d/%m/%Y') - dt.strptime(x, '%d/%m/%Y')).days if type(x) == str else 0 for x in df['launch_date'] ]

df['Display'] = [s[:-1] if type(s) == str else 0 for s in df['Display']]

df['Front Camera'] = [eval(s[:-3]) if type(s) == str else 0 for s in df['Front Camera']]

df['Rear Camera'] = [eval(s[:-3]) if type(s) == str else 0 for s in df['Rear Camera']]

df['Capacity'] = [s[:-2] if type(s) == str else 0 for s in df['Capacity']]

df['Memory'] = [s[:-2] if type(s) == str else 0 for s in df['Memory']]

df['Weigh'] = [s[:-1] if type(s) == str else 0 for s in df['Weigh']]

df['Battery Capacity'] = [s[:-3] if type(s) == str else 0 for s in df['Battery Capacity']]

df.to_csv('fixed_most_columns.csv', encoding='utf_8_sig')