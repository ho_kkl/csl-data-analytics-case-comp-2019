import pandas as pd
import numpy as np
import yaml

df = pd.read_csv('items.csv')

df['spec_name_value'] = [yaml.load(x.replace("'", '"')) for x in df['spec_name_value']]

df_keep = df

df = df_keep

spec_name_value_new = []
for d in df['spec_name_value']:
    tmp = {}
    for i in d:
        new_key = i[:-1]
        tmp[new_key] = d[i]
    spec_name_value_new.append(tmp)
df['spec_name_value'] = spec_name_value_new

df['上市日期'] = ''
df['作業系統'] = ''
df['制式'] = ''
df['顯示屏'] = ''
df['解像度'] = ''
df['處理器'] = ''
df['前鏡頭'] = ''
df['後鏡頭'] = ''
df['容量'] = ''
df['記憶體'] = ''
df['功能'] = ''
df['重量'] = ''
df['尺寸'] = ''
df['電池容量'] = ''

spec_list = ['上市日期', '作業系統', '制式', '顯示屏', '解像度', '處理器', '前鏡頭', '後鏡頭', '容量', '記憶體', '功能', '重量', '尺寸', '電池容量']

for index, row in df.iterrows():
    for item in spec_list:
        if item in row['spec_name_value']:
            df[item][index] = row['spec_name_value'][item]


df.to_csv('final.csv', encoding='utf_8_sig')